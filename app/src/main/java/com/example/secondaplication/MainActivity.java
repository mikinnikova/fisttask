package com.example.secondaplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayDeque;

public class MainActivity extends AppCompatActivity {

    EditText text;
    Button clearBtn;
    Button returnBtn;

    ArrayDeque<Editable> rememberedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.editText);
        clearBtn = (Button) findViewById(R.id.clear);
        returnBtn = (Button) findViewById(R.id.return_btn);

        rememberedText = new ArrayDeque(5);

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rememberedText.add(text.getText());
                // s = text.getText();
                text.setText(" ");
            }

        });
        returnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setText(rememberedText.pollLast());
                // text.setText(s);
            }
        });

    }
}
